# Sebelum running program

    - config database di web-server.yml

  datasource :
    driver-class-name: com.mysql.jdbc.Driver
    url : jdbc:mysql://localhost/dbtechno
    username : techno
    password : 12345

    - mvn clean install

# Running program

    - mvn spring-boot:run

    - buka google chrome masukan "http://localhost:5560/login"