package com.techno.techno.configuration;

import com.techno.techno.handler.CustomAccessDeniedHandler;
import com.techno.techno.handler.CustomAuthenticationFailureHandler;
import com.techno.techno.handler.CustomLogoutSuccessHandler;
import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

@Configuration
@EnableWebSecurity
public class AppSecurityConfiguration extends WebSecurityConfigurerAdapter {
    // @Autowired
    // private UserService userService;

    // @Override
    // protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
    //     auth
    //             .authenticationProvider(authenticationProvider());
    // }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/index").permitAll()
                .antMatchers("/admin/**").hasRole("ADMIN")
                .antMatchers("/anonymous*").anonymous()
                .antMatchers("/supplier*").permitAll()
                .antMatchers("/login*").permitAll()
                .antMatchers("/outlet*").permitAll()
                .antMatchers("/cardtype*").permitAll()
                .antMatchers("/dashboard/**").permitAll()
                .antMatchers("/assets/**").permitAll()
                .antMatchers("/api/**").permitAll()
                .antMatchers("/paymentmethod*").permitAll()
                .antMatchers("/bankaccount*").permitAll()
                .antMatchers("/employee/**").permitAll()
                .antMatchers("/userForm/**").permitAll()
                .antMatchers("/category/**").permitAll()
                .antMatchers("/customer*").permitAll()
                .antMatchers("/mdr*").permitAll()
                .anyRequest().permitAll()
                .and()
                .formLogin()
                .loginPage("/login")
                .loginProcessingUrl("/perform_login")
                .defaultSuccessUrl("/coutlet", true)
                .failureUrl("/login?error=true")
                .failureHandler(authenticationFailureHandler())
                .and()
                .logout()
                .logoutUrl("/perform_logout")
                .deleteCookies("JSESSIONID")
                .logoutSuccessHandler(logoutSuccessHandler());
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    // @Bean
    // public DaoAuthenticationProvider authenticationProvider() {
    //     DaoAuthenticationProvider auth = new DaoAuthenticationProvider();
    //     auth.setUserDetailsService(userService);
    //     auth.setPasswordEncoder(passwordEncoder());
    //     return auth;
    // }

    @Bean
    public AccessDeniedHandler accessDeniedHandler() {
        return new CustomAccessDeniedHandler();
    }

    @Bean
    public LogoutSuccessHandler logoutSuccessHandler() {
        return new CustomLogoutSuccessHandler();
    }

    @Bean
    public AuthenticationFailureHandler authenticationFailureHandler() {
        return new CustomAuthenticationFailureHandler();
    }

     @Bean
     public ModelMapper modelMapper() {
         ModelMapper modelMapper = new ModelMapper();
         modelMapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
         return modelMapper;
     }

}

