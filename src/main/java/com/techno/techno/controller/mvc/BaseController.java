package com.techno.techno.controller.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/")
public class BaseController {

    @GetMapping(value = "dashboard")
    public String dashboard() {
        return "dashboard";
    }

    @GetMapping("permintaanBahanBaku")
    public String permintaanBahanBaku() {
        return "permintaanBahanBaku";
    }

    @GetMapping("pesananPembelian")
    public String pesananPembelian() {
        return "pesananPembelian";
    }

    @GetMapping("laporan")
    public String laporan() {
        return "laporan";
    }

    @GetMapping("penerimaanBarang")
    public String penerimaanBarang() {
        return "penerimaanBarang";
    }

    @GetMapping("pembayaran")
    public String pembayaran() {
        return "pembayaran";
    }

    @GetMapping("login")
    public String login() {
        return "login";
    }


}
