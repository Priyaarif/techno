package com.techno.techno.controller.restful;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import com.techno.techno.controller.mvc.BaseController;
import com.techno.techno.model.DefaultResponse;
import com.techno.techno.model.dto.AddBarangDto;
import com.techno.techno.model.entity.master.Barang;
import com.techno.techno.model.entity.master.User;
import com.techno.techno.service.BarangService;
import com.techno.techno.service.UserService;

@RestController
@RequestMapping("/api/barang")
public class BarangController extends BaseController {
    
    @Autowired
    private BarangService barangService;

    @PostMapping
    public DefaultResponse save(@RequestBody AddBarangDto barang) {
        Barang barangSave = barangService.save(barang);
        DefaultResponse<Barang> response = new DefaultResponse(Boolean.TRUE);
        response.setData(barangSave);
        return response;
    }

    @PutMapping
    public DefaultResponse edit(@RequestBody Barang barang) {
        // di map dulu ke Class User
        Barang barangEdit = barangService.edit(barang);
        DefaultResponse<Barang> response = new DefaultResponse(Boolean.TRUE);
        response.setData(barangEdit);

        return response;
    }

    @GetMapping("{np}")
    public DefaultResponse findByNp(@PathVariable("np") String np){
        List<Barang> result = barangService.findAllByNp(np);
        DefaultResponse<List<Barang>> response = new DefaultResponse(Boolean.TRUE);
        response.setData(result);
        return response;
    }


    @GetMapping("/id/{np}")
    public DefaultResponse findById(@PathVariable("id") Long id){
        Barang result = barangService.findById(id);
        DefaultResponse<Barang> response = new DefaultResponse(Boolean.TRUE);
        response.setData(result);
        return response;
    }

    @GetMapping("/{np}/{namaBarang}")
    public DefaultResponse findByNpAndNamaBarang(@PathVariable("np") String np,
                                                 @PathVariable("namaBarang") String namaBarang){
        Barang result = barangService.findByNpAndNamaBarang(np, namaBarang);
        DefaultResponse<Barang> response = new DefaultResponse(Boolean.TRUE);
        response.setData(result);
        return response;
    }
}
