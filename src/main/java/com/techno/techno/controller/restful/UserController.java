package com.techno.techno.controller.restful;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import com.techno.techno.controller.mvc.BaseController;
import com.techno.techno.model.DefaultResponse;
import com.techno.techno.model.dto.UserDto;
import com.techno.techno.model.entity.master.User;
import com.techno.techno.service.UserService;

@RestController
@RequestMapping("/api/user")
public class UserController extends BaseController {
    
    @Autowired
    private UserService userService;

    @PostMapping
    public DefaultResponse save(@RequestBody User user) {
        User userSave = userService.save(user);
        DefaultResponse<User> response = new DefaultResponse(Boolean.TRUE);
        response.setData(userSave);
        return response;
    }

    @PutMapping
    public DefaultResponse edit(@RequestBody User user) {
        // di map dulu ke Class User
        User userEdit = userService.edit(user);
        DefaultResponse<User> response = new DefaultResponse(Boolean.TRUE);
        response.setData(userEdit);

        return response;
    }

    @GetMapping("{np}")
    public DefaultResponse findByNp(@PathVariable("np") String np){
        UserDto result = userService.findByNp(np);
        DefaultResponse<UserDto> response = new DefaultResponse(Boolean.TRUE);
        response.setData(result);
        return response;
    }

    @GetMapping("/login/{username}/{password}")
    public DefaultResponse findByNp(@PathVariable("username") String username,
                                    @PathVariable("password") String password){
        String result = userService.login(username, password);
        DefaultResponse<String> response = new DefaultResponse(Boolean.TRUE);
        response.setData(result);
        return response;
    }
}
