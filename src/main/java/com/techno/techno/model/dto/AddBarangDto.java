package com.techno.techno.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
public class AddBarangDto {
    @NotNull
    private Long id;

    private String namaBarang;

    private String qty;

    private String satuan;

    private String hargaSatuan;

    private String total;

    private String terimaBarang;

    private String keterangan;

    private String np;


}
