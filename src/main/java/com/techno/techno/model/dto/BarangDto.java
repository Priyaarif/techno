package com.techno.techno.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
public class BarangDto {
    @NotNull
    private Long id;

    private String namaBarang;

    private String qty;

    private String hargaSatuan;

    private String total;

    private String terimaBarang;

    private String keterangan;


}
