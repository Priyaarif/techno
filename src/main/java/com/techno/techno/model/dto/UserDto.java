package com.techno.techno.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

import com.techno.techno.model.entity.master.Barang;

import java.util.Date;
import java.util.List;

@Data
public class UserDto {
    @NotNull
    private Long id;

    private String np;

    private String nama;

    private String bagian;

    private String namaSupplier;

    private String alamatSupplier;

    private String totalHarga;

    private String status;

    private String keterangan;

    private List<Barang> barang;


}
