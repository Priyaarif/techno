package com.techno.techno.model.entity.master;

import lombok.Data;

import javax.persistence.*;

import com.techno.techno.model.CommonEntity;

@Data
@Entity
@Table(name= Barang.TABLE_NAME)
public class Barang extends CommonEntity {
    public static final String TABLE_NAME = "BARANG";
    private static final long serialVersionUID = -7054659686634430552L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.TABLE, generator = TABLE_NAME)
    @TableGenerator(name = TABLE_NAME, table = "T_SEQUENCE", pkColumnName = "SEQ_NAME", pkColumnValue = TABLE_NAME, valueColumnName = "SEQ_VAL", allocationSize = 1, initialValue = 1)
    private Long id;

    @Column(name = "Nama_Barang", nullable = false)
    private String namaBarang;

    @Column(name = "Qty")
    private String qty;

    @Column(name = "Satuan")
    private String satuan;

    @Column(name = "Harga_Satuan")
    private String hargaSatuan;

    @Column(name = "Total")
    private String total;

    @ManyToOne
    @JoinColumn(name = "User_Id")
    private User user;

    @Column(name = "Terima_Barang")
    private String terimaBarang;

    @Column(name = "Keterangan")
    private String keterangan;


}