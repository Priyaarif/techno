package com.techno.techno.model.entity.master;

import lombok.Data;

import javax.persistence.*;

import com.techno.techno.model.CommonEntity;

@Data
@Entity
@Table(name= User.TABLE_NAME)
public class User extends CommonEntity {
    public static final String TABLE_NAME = "USER";
    private static final long serialVersionUID = -7054659686634430552L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.TABLE, generator = TABLE_NAME)
    @TableGenerator(name = TABLE_NAME, table = "T_SEQUENCE", pkColumnName = "SEQ_NAME", pkColumnValue = TABLE_NAME, valueColumnName = "SEQ_VAL", allocationSize = 1, initialValue = 1)
    private Long id;

    @Column(name = "Nomer_pesanan", nullable = false)
    private String np;

    @Column(name = "Nama")
    private String nama;

    @Column(name = "Bagian")
    private String bagian;

    @Column(name = "Nama_Supplier")
    private String namaSupplier;

    @Column(name = "Alamat_Supplier")
    private String alamatSupplier;

    @Column(name = "Total_Harga")
    private String totalHarga;

    @Column(name = "Status")
    private String status;

    @Column(name = "Keterangan")
    private String keterangan;

}