package com.techno.techno.model.entity.master;

import lombok.Data;

import javax.persistence.*;

import com.techno.techno.model.CommonEntity;

@Data
@Entity
@Table(name= UserLogin.TABLE_NAME)
public class UserLogin extends CommonEntity {
    public static final String TABLE_NAME = "USER_LOGIN";
    private static final long serialVersionUID = -7054659686634430552L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.TABLE, generator = TABLE_NAME)
    @TableGenerator(name = TABLE_NAME, table = "T_SEQUENCE", pkColumnName = "SEQ_NAME", pkColumnValue = TABLE_NAME, valueColumnName = "SEQ_VAL", allocationSize = 1, initialValue = 1)
    private Long id;

    @Column(name = "Username", nullable = false)
    private String username;

    @Column(name = "Password")
    private String password;

}