package com.techno.techno.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

import com.techno.techno.model.entity.master.Barang;
import com.techno.techno.model.entity.master.User;

@Repository
public interface BarangRepository extends JpaRepository<Barang, String> {

    Barang findById(Long id);

    List<Barang> findByUser(User user);
}
