package com.techno.techno.repository;

import com.techno.techno.model.entity.master.UserLogin;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LoginRepository extends JpaRepository<UserLogin, String> {

    UserLogin findById(Long id);

    UserLogin findByUsername(String username);
}