package com.techno.techno.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.techno.techno.model.entity.master.User;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

    User findById(Long id);

    User findByNp(String np);
}
