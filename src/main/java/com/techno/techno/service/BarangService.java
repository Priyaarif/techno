package com.techno.techno.service;

import java.util.List;

import com.techno.techno.model.dto.AddBarangDto;
import com.techno.techno.model.entity.master.Barang;

public interface BarangService {

    Barang save(AddBarangDto dto);
    Barang edit(Barang barang);
    Barang findById(Long id);
    List<Barang> findAllByNp(String Np);
    Barang findByNpAndNamaBarang(String np, String namaBarang);
}