package com.techno.techno.service;

import java.util.Date;
import java.util.List;

import com.techno.techno.model.dto.AddBarangDto;
import com.techno.techno.model.entity.master.Barang;
import com.techno.techno.model.entity.master.User;
import com.techno.techno.repository.BarangRepository;
import com.techno.techno.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class BarangServiceImpl implements BarangService {

    @Autowired
    BarangRepository barangRepository;

    @Autowired
    UserRepository userRepository;

    @Override
    public Barang save(AddBarangDto dto) {
        Date currentDate = new Date();
        User user = userRepository.findByNp(dto.getNp());
        Barang barang = new Barang();
        barang.setCreatedOn(currentDate);
        barang.setModifiedOn(currentDate);
        barang.setUser(user);
        barang.setHargaSatuan(dto.getHargaSatuan());
        barang.setKeterangan(dto.getKeterangan());
        barang.setNamaBarang(dto.getNamaBarang());
        barang.setQty(dto.getQty());
        barang.setSatuan(dto.getSatuan());
        barang.setTerimaBarang(dto.getTerimaBarang());
        barang.setTotal(dto.getTotal());
        return barangRepository.save(barang);
    }

    @Override
    public Barang edit(Barang barangEdit) {
        Date currentDate = new Date();
        // dapatkan dulu data aslinya
        Barang barang = barangRepository.findById(barangEdit.getId());

        barang.setModifiedOn(currentDate);
        barang.setHargaSatuan(barangEdit.getHargaSatuan());
        barang.setKeterangan(barangEdit.getKeterangan());
        barang.setNamaBarang(barangEdit.getNamaBarang());
        barang.setQty(barangEdit.getQty());
        barang.setSatuan(barangEdit.getSatuan());
        barang.setTerimaBarang(barangEdit.getTerimaBarang());
        barang.setTotal(barangEdit.getTotal());
        // mappingkan yang mau di eddit
        barangRepository.save(barang);
        return barang;
    }

    @Override
    public Barang findById(Long id) {
        // TODO Auto-generated method
        Barang barang = barangRepository.findById(id);

        return barang;
    }

    @Override
    public List<Barang> findAllByNp(String np) {
        // TODO Auto-generated method stub
        User user = userRepository.findByNp(np);
        List<Barang> barangList = barangRepository.findByUser(user);
        return barangList;
    }

    @Override
    public Barang findByNpAndNamaBarang(String np, String namaBarang) {
        // TODO Auto-generated method stub
        User user = userRepository.findByNp(np);
        List<Barang> barangList = barangRepository.findByUser(user);
        Barang result = new Barang();
        barangList.forEach(barang -> {
            if (barang.getNamaBarang().equals(namaBarang)){
                result.setId(barang.getId());
                result.setHargaSatuan(barang.getHargaSatuan());
                result.setKeterangan(barang.getKeterangan());
                result.setNamaBarang(barang.getNamaBarang());
                result.setQty(barang.getQty());
                result.setSatuan(barang.getSatuan());
                result.setTerimaBarang(barang.getTerimaBarang());
                result.setTotal(barang.getTotal());
            }
        });
        return result;
    }
}