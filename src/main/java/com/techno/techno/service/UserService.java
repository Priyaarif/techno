package com.techno.techno.service;

import com.techno.techno.model.dto.UserDto;
import com.techno.techno.model.entity.master.User;

public interface UserService {

    User save(User user);
    User edit(User userEdit);
    UserDto findByNp(String np);
    String login(String username, String Password);
}