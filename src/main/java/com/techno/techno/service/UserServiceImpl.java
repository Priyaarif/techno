package com.techno.techno.service;

import java.util.Date;
import java.util.List;

import com.techno.techno.model.dto.BarangDto;
import com.techno.techno.model.dto.UserDto;
import com.techno.techno.model.entity.master.Barang;
import com.techno.techno.model.entity.master.User;
import com.techno.techno.model.entity.master.UserLogin;
import com.techno.techno.repository.BarangRepository;
import com.techno.techno.repository.LoginRepository;
import com.techno.techno.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    BarangRepository barangRepository;

    @Autowired
    LoginRepository loginRepository;

    @Override
    public User save(User user) {
        Date currentDate = new Date();
        user.setCreatedOn(currentDate);
        user.setModifiedOn(currentDate);
        return userRepository.save(user);
    }

    @Override
    public User edit(User userEdit) {
        Date currentDate = new Date();
        // dapatkan dulu data aslinya
        User user = userRepository.findByNp(userEdit.getNp());

        user.setModifiedOn(currentDate);
        user.setAlamatSupplier(userEdit.getAlamatSupplier());
        user.setBagian(userEdit.getBagian());
        user.setKeterangan(userEdit.getKeterangan());
        user.setNama(userEdit.getNama());
        user.setNamaSupplier(userEdit.getNamaSupplier());
        user.setTotalHarga(userEdit.getTotalHarga());
        user.setStatus(userEdit.getStatus());
        // mappingkan yang mau di eddit
        userRepository.save(user);
        return user;
    }

    @Override
    public UserDto findByNp(String np) {
        // TODO Auto-generated method 
        User user = userRepository.findByNp(np);
        UserDto result = new UserDto();
        result.setAlamatSupplier(user.getAlamatSupplier());
        result.setBagian(user.getBagian());
        result.setId(user.getId());
        result.setKeterangan(user.getKeterangan());
        result.setNama(user.getNama());
        result.setNamaSupplier(user.getNamaSupplier());
        result.setNp(user.getNp());
        result.setStatus(user.getStatus());
        result.setTotalHarga(user.getTotalHarga());
        List<Barang> barang = barangRepository.findByUser(user);
        result.setBarang(barang);
        
        return result;
    }

    @Override
    public String login(String username, String password) {

        UserLogin userLogin = loginRepository.findByUsername(username);
        if(!userLogin.getPassword().equals(password)){
            throw new java.lang.NullPointerException();
        }
        // TODO Auto-generated method stub
        return "sukses";
    }
}